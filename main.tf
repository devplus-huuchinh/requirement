provider "aws"{
  region = var.region
}

data "aws_caller_identity" "current" {}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-20240411"]
  }

  # filter {
  #   name = "virtualization-type"
  #   values = ["hvm"]
  # }

  owners = ["amazon"]
}

module "networking" {
  source = "./modules/Networking"
  name = var.name
  Environment = var.Environment
  region = var.region
  cidr_block = var.cidr_block
  public_subnets = var.public_subnets
  private_subnets = var.private_subnets
}

module "ec2" {
  depends_on = [module.networking]
  source = "./modules/EC2"
  name = var.name
  Environment = var.Environment
  region = var.region
  ami = data.aws_ami.ubuntu.id
  vpc = module.networking.aws_vpc_id
  public_subnets = module.networking.aws_public_subnets
  private_subnets = module.networking.aws_private_subnet
  allow_http = module.networking.allow_http
  alb_tg = module.networking.tg_arn
}