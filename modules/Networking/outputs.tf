output "aws_vpc_id" {
  value       = aws_vpc.proto.id
  description = "The name of vpc"
}

output "aws_private_subnet" {
  value       = aws_subnet.proto_private[*].id
  description = "The name of all Private Subnets"
}

output "aws_public_subnets" {
    value = aws_subnet.proto_public[*].id
    description = "The name of all Public Subnets"
}

output "aws_route_table" {
    value = aws_route_table.public.id
    description = "The name of route table"
}

output "aws_internet_gateway" {
    value = aws_internet_gateway.proto_ig.id
    description = "The name of internet gateway"
}

output "aws_eip" {
    value = aws_eip.proto_eip[*].id
    description = "The name of elastic IP"
}

output "aws_nat_gateway" {
    value = aws_nat_gateway.proto_nat[*].id
    description = "The name of NAT gateway"
}

output "allow_http" {
    value = aws_security_group.allow_http.id
    description = "Loadbalancing Security Group"
}

output "alb_dns" {
    value = aws_lb.alb.dns_name
    description = "Loadblancing dns name"
}

output "tg_arn" {
  value       = aws_lb_target_group.alb_tg.arn
  description = "Target group arn"
}
