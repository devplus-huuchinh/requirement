locals {
  availability_zones = ["${var.region}a","${var.region}b"]
}

resource "aws_vpc" "proto"{
    cidr_block = var.cidr_block
    instance_tenancy = "default"
    tags = {
        Name = "${var.name}-vpc"
        Environment = var.Environment
    }
}

resource "aws_subnet" "proto_private"{
    vpc_id = aws_vpc.proto.id
    count = length(local.availability_zones)
    cidr_block = element(var.private_subnets, count.index)
    availability_zone       = local.availability_zones[count.index]

    tags = {
        Name = "${var.name}-private-subnet-${count.index + 1}"
        Environment = var.Environment
    }
} 
resource "aws_subnet" "proto_public"{
    vpc_id = aws_vpc.proto.id
    count = length(local.availability_zones)
    cidr_block = element(var.public_subnets, count.index)
    map_public_ip_on_launch = true
    availability_zone       = local.availability_zones[count.index]

    tags = {
        Name = "${var.name}-public-subnet-${count.index + 1}"
        Environment = var.Environment
    }
}

# Internet gateway

resource "aws_internet_gateway" "proto_ig" {
  vpc_id = aws_vpc.proto.id

  tags = {
    Name = "${var.name}-internet-gateway"
    Environment = var.Environment
  }
}

# Route table

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.proto.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.proto_ig.id
  }

  tags = {
    Name = "${var.name}-route-table"
    Environment = var.Environment
  }
}

resource "aws_route_table_association" "public_association" {
  for_each       = { for k, v in aws_subnet.proto_public : k => v }
  subnet_id      = each.value.id
  route_table_id = aws_route_table.public.id  
}

# Elastic IP for NAT gateway

resource "aws_eip" "proto_eip" {
  domain = "vpc"
  count = length(local.availability_zones)
  tags = {
    Name = "${var.name}-NAT-gateway-Elastic_IP-${count.index}"
    Environment = var.Environment
  }
}

# Route table for private subnets
resource "aws_route_table" "private" {
  vpc_id = aws_vpc.proto.id
  count = length(local.availability_zones)
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.proto_nat[count.index].id
  }
  depends_on = [ aws_nat_gateway.proto_nat ]
}

# Associate private subnets with route table
resource "aws_route_table_association" "private_subnet_association" {
  count = length(local.availability_zones)
  subnet_id = aws_subnet.proto_private[count.index].id
  route_table_id = aws_route_table.private[count.index].id
}

# NAT gateway -public type
resource "aws_nat_gateway" "proto_nat" {
  count = length(local.availability_zones)
  allocation_id = aws_eip.proto_eip[count.index].id
  subnet_id     = aws_subnet.proto_public[count.index].id

  tags = {
    Name = "${var.name}-NAT-gateway-${count.index}"
    Environment = var.Environment
  }
  depends_on = [aws_internet_gateway.proto_ig]
}

# ALB

resource "aws_lb_target_group" "alb_tg" {
  # depends_on = [aws_security_group.alb_sg]
  name        = "${var.name}-lb-alb-tg"
  # target_type = "alb" default target type - Instance
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.proto.id
}

resource "aws_security_group" "alb_sg" {
  vpc_id = aws_vpc.proto.id
  name = "ALB-security griup"
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_http" {
  vpc_id = aws_vpc.proto.id
  name = "allow_http"
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    # cidr_blocks = ["0.0.0.0/0"]
    security_groups = [aws_security_group.alb_sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb" "alb"{
  name = "${var.name}-alb"
  load_balancer_type = "application"
  security_groups = [aws_security_group.alb_sg.id]
  subnets = [for subnet in aws_subnet.proto_public : subnet.id]

}

resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port = "80"
  protocol = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_tg.arn
  }
}

