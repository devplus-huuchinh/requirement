variable "Environment" {
  type = string
}

variable "name" {
  type = string
}

variable "region" {
  type = string
}

variable "ami" {
  type        = string
}

variable "vpc" {
  type = string
}

variable "public_subnets" {
  type = list
}

variable "private_subnets" {
  type = list
}

variable "allow_http" {
  type = string
}

variable "alb_tg" {
  type = string
}
