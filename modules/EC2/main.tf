locals {
  user_data =  <<-EOF
            #!/bin/bash
            # Update the package index
            apt-get update

            # Upgrade installed packages
            apt-get upgrade -y

            # Install common packages
            apt-get install -y build-essential git curl

            # Install Nginx
            apt-get install -y nginx
            
            # sudo apt-get install -y git
            cd /var/www/html/
            git clone https://gitlab.com/devplus-huuchinh/demo.git
            mv var/www/html/demo/* /var/www/html/

            # Start Nginx service
            systemctl start nginx
            systemctl enable nginx
            EOF
}

locals {
  base64_user_data = base64encode(local.user_data)
}
# Autoscaling
# Define the launch template
resource "aws_launch_template" "server_asg_lt" {
  name_prefix   = "${var.name}-asg-lt"
  image_id      = var.ami  # Replace with your desired AMI
  instance_type = "t2.micro"

  network_interfaces {
    associate_public_ip_address = false
    security_groups             = [var.allow_http]
  }

  user_data = local.base64_user_data

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "server_asg" {
  depends_on = [aws_launch_template.server_asg_lt]
  vpc_zone_identifier = var.private_subnets
  desired_capacity   = 1
  max_size           = 2
  min_size           = 1
  target_group_arns = [var.alb_tg]
   launch_template {
    id      = aws_launch_template.server_asg_lt.id
    version = "$Latest"
  }
}
