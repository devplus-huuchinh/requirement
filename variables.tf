variable "Environment" {
  type = string
  default = "dev"
}

variable "name" {
  type = string
  default = "delta"
}

variable "region" {
  type = string
  default = "us-east-1"
}

variable "cidr_block" {
  type = string
  default = "10.0.0.0/16"
}

variable "public_subnets" {
  type = list
  default = ["10.0.1.0/24", "10.0.2.0/24"]
}

variable "private_subnets" {
  type = list
  default = ["10.0.3.0/24", "10.0.4.0/24"]
}